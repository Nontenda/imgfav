﻿using System;
using Newtonsoft.Json.Linq;
using System.Diagnostics;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace imgFav
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void LoginButton_OnClick(object sender, RoutedEventArgs e)
        {
            Process.Start("https://api.imgur.com/oauth2/authorize?client_id=YOUR_CLIENT_ID&response_type=pin");

            LoginButton.Visibility = Visibility.Hidden;
            LoginText.Visibility = Visibility.Visible;
            LoginBox.Visibility = Visibility.Visible;
        }


        private void LoginBox_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Enter) return;

            if (LoginBox.Text.Length <= 4) return;


            var webClient = new WebClientSSL();
            webClient.QueryString.Add("pin", LoginBox.Text);
            var loginJson = webClient.DownloadString("A WEB SERVICE TO GIVE YOU THE PIN CODE/login");

            if (loginJson.Contains("fatal error"))
            {
                MessageBox.Show("Fatal error while login, sorry", "Fatal error");
                return;
            }

            // Alright for getting access_code
            try
            {
                dynamic imgurAnswer = JObject.Parse(loginJson);

                new GetFavorites(imgurAnswer.access_token.Value.ToString(), imgurAnswer.refresh_token.Value.ToString(), DateTime.Now.AddSeconds(Double.Parse(imgurAnswer.expires_in.Value.ToString())), imgurAnswer.account_username.Value.ToString()).Show();

                Close();
            }
            catch
            {
                MessageBox.Show("Fatal error while parsing answer from imgur, sorry", "Fatal error");
                return;
            }
        }
    }
}
