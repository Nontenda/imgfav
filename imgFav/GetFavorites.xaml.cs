﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json.Linq;

using WinForms = System.Windows.Forms;

namespace imgFav
{
    /// <summary>
    /// Logique d'interaction pour GetFavorites.xaml
    /// </summary>
    public partial class GetFavorites : Window
    {
        private readonly string _accessToken;
        private readonly string _refreshToken;
        private readonly DateTime _expired;
        private readonly string _accountUsername;
        private string _path = "";

        private enum Progress
        {
            GotFavorites = -2,
            DownloadingFavorites = -1,
            Finished = 0,
        };

        private readonly BackgroundWorker _loadFavorites = new BackgroundWorker();
        private int _totalImages;

        public GetFavorites(string accessToken, string refreshToken, DateTime expired, string accountUsername)
        {
            if (accessToken == null) throw new ArgumentNullException("accessToken");
            if (refreshToken == null) throw new ArgumentNullException("refreshToken");
            if (accountUsername == null) throw new ArgumentNullException("accountUsername");
            _accessToken = accessToken;
            _refreshToken = refreshToken;
            _expired = expired;
            _accountUsername = accountUsername;

            var fbd = new WinForms.FolderBrowserDialog();
            bool correctFolder = false;
            while (!correctFolder)
            {
                WinForms.DialogResult result = fbd.ShowDialog();

                if (result == WinForms.DialogResult.OK)
                {
                    correctFolder = true;
                }
                else
                {
                    MessageBox.Show("Choose a correct folder.");
                }
            }

            _path = fbd.SelectedPath;

            InitializeComponent();

            _loadFavorites.DoWork += LoadFavoritesOnDoWork;
            _loadFavorites.RunWorkerCompleted += _loadFavorites_RunWorkerCompleted;
            _loadFavorites.ProgressChanged += _loadFavorites_ProgressChanged;
            _loadFavorites.WorkerReportsProgress = true;
            _loadFavorites.RunWorkerAsync();
        }

        void _loadFavorites_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            switch (e.ProgressPercentage)
            {
                case (int)Progress.GotFavorites:
                    MyFadingText.Text = "Downloading Favorites";
                    break;

                case (int)Progress.Finished:
                    MyFadingText.Text = "COMPLETED, go to : " + _path;
                    break;

                default:
                    if (e.ProgressPercentage < -999)
                    {
                        _totalImages = (e.ProgressPercentage*-1) - 1000;
                    }
                    else if (e.ProgressPercentage > 999)
                    {
                        MyFadingText.Text = "Downloading Favorites : " + (e.ProgressPercentage - 1000) + "/" + _totalImages;
                    }
                    break;
            }
        }

        void _loadFavorites_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
        }

        private void LoadFavoritesOnDoWork(object sender, DoWorkEventArgs doWorkEventArgs)
        {
            var webClient = new WebClient();
            webClient.Headers.Add("Authorization", "Bearer " + _accessToken);
            var favorites = webClient.DownloadString("https://api.imgur.com/3/account/" + _accountUsername + "/favorites");

            dynamic imgurAnswer = JObject.Parse(favorites);

            var favoritesArray = JArray.Parse(imgurAnswer.data.ToString());

            _loadFavorites.ReportProgress((int)Progress.GotFavorites);

            var favoritesToDownload = new List<KeyValuePair<String, String>>();

            foreach (var favorite in favoritesArray)
            {
                if (favorite.is_album.ToString().ToLower() == "false")
                {
                    favoritesToDownload.Add(new KeyValuePair<string, string>(favorite.link.ToString(), favorite.title.ToString()));
                }
            }

            _loadFavorites.ReportProgress(-1000 - favoritesToDownload.Count);

            var client = new WebClient();
            var i = 0;
            foreach (var favorite in favoritesToDownload)
            {
                client.DownloadFile(favorite.Key, _path + "\\" + favorite.Value.Replace("\\", "").Replace("/", "").Replace(":", "").Replace("*", "").Replace("?", "").Replace("<", "").Replace(">", "").Replace("\"", "").Replace("|", "") + System.IO.Path.GetExtension(favorite.Key));
                i++;
                _loadFavorites.ReportProgress(1000 + i);
            }

            _loadFavorites.ReportProgress((int)Progress.Finished);
        }
    }
}
